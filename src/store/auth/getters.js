export default {
  getUserName: ({ userName }) => userName,
  getUserEmail: ({ userEmail }) => userEmail,
  getUserToken: ({ userToken }) => userToken,
}