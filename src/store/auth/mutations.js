export default {
  SET_USER_DATA: (state, userData) => {
    state.userName = userData.nome
    state.userEmail = userData.email
    state.userToken = userData.token

    window.localStorage.setItem('userData', JSON.stringify(userData))
  },

  RESET_USER_DATA: (state) => {
    state.userName = null
    state.userEmail = null
    state.userToken = null

    window.localStorage.removeItem('userData')
  }
}