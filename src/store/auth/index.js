import { getUserData } from "./../../util/DataFromStorage";

import getters from './getters'
import mutations from './mutations'
import actions from './actions'


const user = getUserData()

export default {
  namespaced: true,
  state: {
    userName: user ? user.nome : null,
    userEmail: user ? user.email : null,
    userToken: user ? user.token : null
  },
  getters,
  mutations,
  actions
}