import Message from './../../util/Message'

import { HTTP } from './../../service'
import { LOGIN } from "./../../service/endpoints";

export default {
  signin: async ({ commit }, payload) => {
    try {
      const { email, password } = payload
      
      if (!email || !password) {
        throw new Error('INVALID_DATA')
      }

      const { status, data } = await HTTP.post(
        LOGIN,
        { email, senha: password }
      )
      
      if (status !== 200 || !data.data ) {
        throw new Error('LOGIN_ERROR')
      }

      commit('SET_USER_DATA', data.data)
      
      return new Message(true, 'Login realizado com sucesso!')
    } catch (error) {
      console.error(error)
      return new Message(false, 'Login indisponível. Tente mais tarde.')
    }
  },

  signout: async ({ commit }) => {
    try {
      commit('RESET_USER_DATA')
      return new Message(true, 'Usuário deslogado com sucesso')
    } catch (error) {
      console.error(error)
      return new Message(false, 'Erro ao tentar sair da aplicação')
    }
  }
}