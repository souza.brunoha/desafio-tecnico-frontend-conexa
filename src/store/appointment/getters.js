export default {
  getAppointmentList: ({ appointmentList }) => appointmentList,
  getAppointmentSelected: ({ appointmentSelected }) => appointmentSelected
}