import Message from './../../util/Message'

import { HTTP } from "./../../service";
import { LISTAR_CONSULTAS, DETALHAR_CONSULTA, CRIAR_CONSULTA } from "./../../service/endpoints";

export default {
  listAppointments: async ({ commit }) => {
    try {
      commit('SET_APPOINTMENT_LIST', [])
      const { data } = await HTTP.get(LISTAR_CONSULTAS)

      commit('SET_APPOINTMENT_LIST', data.data)

      return new Message(true, 'Consultas listadas com sucesso')
    } catch (error) {
      console.error(error)
      return new Message(false, 'Não foi possível listar consultas')
    }
  },

  getAppointment: async ({ commit }, payload) => {
    try {
      commit('SET_APPOINTMENT', null)
      const { id } = payload

      if (!id) {
        throw new Error('PARAM_NOT_FOUND')
      }

      const { data } = await HTTP.get(DETALHAR_CONSULTA({ idConsulta: id }))

      commit('SET_APPOINTMENT', data.data)

      return new Message(true, 'Consulta carregada com sucesso')
    } catch (error) {
      let msg = 'Erro ao carregar informações'

      if (error.response && error.response.status === 404) {
        msg = 'Consulta não encontrada'
      }
      
      return new Message(false, msg)
    }
  },

  createAppointment: async (_, payload) => {
    try {
      const {
        appointmentDate,
        doctorId,
        observation,
        pacient
      } = payload

      const {data} = await HTTP.post(CRIAR_CONSULTA, {
        dataConsulta: appointmentDate,
        idMedico: doctorId,
        observacao: observation,
        paciente: pacient
      })

      return new Message(true, `Consulta marcada com sucesso para o ${data.data.medico.nome} no dia ${data.data.dataConsulta}`)
    } catch (error) {
      console.error(error)
      return new Message(false, 'Erro ao criar uma consulta. Tente novamente mais tarde.')
    }
  }
}