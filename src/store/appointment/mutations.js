export default {
  SET_APPOINTMENT_LIST: (state, appointmentList) => {
    state.appointmentList = appointmentList
  },

  SET_APPOINTMENT: (state, appointment) => {
    state.appointmentSelected = appointment
  }
}