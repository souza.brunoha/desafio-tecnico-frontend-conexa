import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth'
import appointment from './appointment'

Vue.use(Vuex)

const modules = {
  auth,
  appointment
}

export default new Vuex.Store({ modules })
