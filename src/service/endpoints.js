export const CRIAR_CONSULTA = `/consulta`
export const DETALHAR_CONSULTA = ({ idConsulta }) => `/consulta/${idConsulta}`
export const LISTAR_CONSULTAS = `/consultas`
export const LOGIN = `/login`