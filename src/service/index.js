import Axios from 'axios'

import { getUserData } from "./../util/DataFromStorage";

const HTTP = Axios.create({
  baseURL: 'http://desafio.conexasaude.com.br/api',
})

HTTP.interceptors.request.use((request) => {
  const userData = getUserData()

  if (userData && userData.token) {
    request.headers['Authorization'] = `Bearer ${userData.token}`
  }

  return request
})

export { HTTP }