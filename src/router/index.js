import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Auth from './../layouts/Auth'
import Default from './../layouts/Default'

import notFound from './../pages/errors/notFound'

import publicRoutes from './publicRoutes'
import privateRoutes from './privateRoutes'

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Auth,
      children: publicRoutes
    },
    {
      path: '/dashboard',
      component: Default,
      children: privateRoutes
    },
    {
      path: '*',
      component: notFound
    }
  ]
})

export default router