import login from './../pages/auth/login'

import { validatePublicRoutes } from "./permissions";

const routes = [
  { 
    path: '', 
    name: 'login',
    component: login,
    beforeEnter: validatePublicRoutes,
  }
]

export default routes