import create from './../pages/appointment/create'
import detail from './../pages/appointment/detail'
import list from './../pages/appointment/list'

import { validatePrivateRoutes } from "./permissions";

const routes = [
  {
    path: '',
    component: list,
    name: 'list'
  },
  {
    path: 'create',
    component: create,
    name: 'create'
  },
  {
    path: 'appointment/:id',
    component: detail,
    name: 'detail'
  },
]

export default routes.map(r => ({
  ...r,
  beforeEnter: validatePrivateRoutes
}))