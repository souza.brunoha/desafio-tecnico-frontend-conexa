import store from './../store'

export const validatePrivateRoutes = (_, __, next) => {
  const token = store.getters['auth/getUserToken']

  if (token) {
    next()
  } else {
    next({ name: 'login' })
  }
}

export const validatePublicRoutes = (_, __, next) => {
  const token = store.getters['auth/getUserToken']

  if (token) {
    next({ name: 'list' })
  } else {
    next()
  }
}