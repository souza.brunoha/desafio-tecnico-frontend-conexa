import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import { localize } from "vee-validate";
import pt_BR from 'vee-validate/dist/locale/pt_BR.json'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// import 'bootstrap-vue/src/icons.scss'

import './assets/scss/main.scss'

import App from './App.vue'
import store from './store'
import router from './router'

localize('pt_BR', pt_BR)

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
