export const getUserData = () => {
  const localStorage = window.localStorage.getItem('userData')

  if (!localStorage) return null

  try {
    const data = JSON.parse(localStorage)
    return data
  } catch {
    return null
  }
}