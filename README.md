# desafio-tecnico-frontend-conexa

## Instalação das dependências
```
npm install
```

### Compilar e hot-reload para ambiente de desenvolvimento
```
npm run serve
```

### Compilar e minificar projeto para produção
```
npm run build
```

### Encontrar correções pendentes
```
npm run lint
```